#!/usr/bin/perl
# Author Mauro Pizzamiglio
# Simple "reminder"
# license: WTFPL

use strict;
use warnings;
use feature "switch";
use IO::File;

sub add($) {
  my $project_name = shift;
  my $fh = new IO::File "$ENV{HOME}/.pm/prjs/$project_name", "w";
  if (defined $fh) {
    print "$project_name have been created\n";
    $fh->close;
  }
}

sub todo($$) {
  my $project_name = shift;
  my $todo         = shift;
  my $fh = new IO::File "$ENV{HOME}/.pm/prjs/$project_name", O_WRONLY|O_APPEND; 

  if (defined $fh) {
    print $fh "$todo\n";
    $fh->close;
    print "todo added to $project_name\n";
  } else {
    print "seems $project_name does not exists\n";
  }
}

sub view {
  my @files = <$ENV{HOME}/.pm/prjs/*>;
  my $c = 0;
  print "\nProjects list:\n";
  foreach my $file (@files) {
    print "\t" . $c++ . " $file\n";
  }
  print "View #";
  my $choose = <STDIN>;
  chomp $choose;
  die("That file does not exists\n") if ($choose gt @files);
  my $fh = new IO::File $files[$choose], "r";
  if (defined $fh) {
    print <$fh>;
    $fh->close;
  }
}

sub list {
  my @files = <$ENV{HOME}/.pm/prjs/*>;
  foreach my $file (@files) {
    print "* $file\n";
  }
}

sub delp {
  my @files = <$ENV{HOME}/.pm/prjs/*>;
  my $c = 0;
  foreach my $file (@files) {
    print $c++ . " $file\n";
  }
  print "Delete #";
  my $choose = <STDIN>;
  chomp $choose;
  die("That file does not exists\n") if ($choose gt @files);
  unlink $files[$choose];
}

sub delt($) {
  my $project_name = shift;
  my $c = 0;
  my @file;
  my $fn = "$ENV{HOME}/.pm/prjs/$project_name";
  open FH, $fn;
  foreach my $line (<FH>) {
    chomp $line;
    push @file, $line;
    print $c++ . " $line\n";
  }
  print "Delete todo #";
  my $choose = <STDIN>;
  unlink $fn;
  my $fh = new IO::File $fn, "w";
  $c = 0;
  foreach my $line (@file) {
    chomp $line;
    print $fh "$line\n" if($c != $choose);
    $c++;
  }
  $fh->close;
}

sub prompt {
  print "\nCommands\n";
  print "add <project name>\n";
  print "todo <project name> <todo>\n";
  print "view\n";
  print "list\n";
  print "delp\n";
  print "delt <project name>\n"
}

my $command = shift;


given ($command) {
  when ("add") { add (shift); }
  when ("todo") { todo (shift, shift); }
  when ("view") { view; }
  when ("delp") { delp; }
  when ("delt") { delt (shift); }
  when ("list") { list; }
  default { prompt; }
}
